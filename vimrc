"
" plugins
"

" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('~/.vim/plugged')
" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" prettier formatter
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue'] }

Plug 'airblade/vim-gitgutter'

" syntax checking for _everything_
Plug 'vim-syntastic/syntastic'

" rust
Plug 'rust-lang/rust.vim'
" toml syntax highlighting
Plug 'cespare/vim-toml'
" rust autocomplete
Plug 'racer-rust/vim-racer'

Plug 'chriskempson/base16-vim'

Plug 'editorconfig/editorconfig-vim'

Plug 'mattn/emmet-vim'

" Initialize plugin system
call plug#end()

"
" end of plugin section
"

if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif

map <C-o> :GFiles<CR>

" Ctrl + N -> Nerdtree
map <C-n> :NERDTreeToggle<CR>
" close vim if nerdtree is only window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" open nerdtree if opening a dir
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

let g:airline_powerline_fonts = 1
let g:airline_left_sep = ''
let g:airline_right_sep = ''

" emmet starting key
let g:user_emmet_leader_key='<C-Z>'

set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux

set number
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
syntax on
"set t_Co=256  " vim-monokai now only support 256 colours in terminal.
"let g:monokai_term_italic = 1
"let g:monokai_gui_italic = 1

" source: https://github.com/mgechev/dotfiles/blob/master/.vimrc
if &term =~ '256color'
  " disable Background Color Erase (BCE) so that color schemes
  " render properly when inside 256-color tmux and GNU screen.
  " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
endif

" Scroll 8 lines at a time
set scrolljump=8
" Highlight searches
set hlsearch
" Ignore case of searches
set ignorecase
" Highlight dynamically as pattern is typed
set incsearch
" Always show status line
set laststatus=2
" Disable error bells
set noerrorbells
" Don’t reset cursor to start of line when moving around.
set nostartofline
" Show the cursor position
set ruler
" Don’t show the intro message when starting Vim
set shortmess=atI
" Show the current mode
set showmode
" Show the filename in the window titlebar
set title
" Show the (partial) command as it’s being typed
set showcmd
" Start scrolling three lines before the horizontal window border
set scrolloff=3

" Optimize for fast terminal connections
set ttyfast

" should restore term title (without it the tab title is 'Thanks for flying with Vim')
let &titleold=getcwd()

if has("statusline")
 set statusline=%<%f\ %h%m%r%=%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\ \"}%k\ %-14.(%l,%c%V%)\ %P
endif

" <F5> -> delete all trailing whitespaces
:nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" autoformat rust code on save
let g:rustfmt_autosave = 1

map <C-P> :FZF <CR>

set wrap
set textwidth=120
set colorcolumn=121

" save buffer before opening a racer go-to-def
set hidden
let g:racer_cmd = "/home/broj/.cargo/bin/racer"

au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap <leader>gd <Plug>(rust-doc)

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)
